package com.qvolta.security;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import java.security.InvalidParameterException;

/**
 * Created by egorsarnavsky on 30.01.2018.
 */


public class ProgressView extends View {

    private static final int DEFAULT_STROKE_WIDTH = 21;
    private static final int DEFAULT_BORDER_WIDTH = 9;
    private static final int DEFAULT_MAX_VALUE = 60;
    private static final int DEFAULT_SHOW_LEFT_FROM = 15;
    private static final int DEFAULT_PERCENT = 0;
    private static final float[] SWEEP_POSITIONS = {0, 0.5f, 1f};
    private int mMax = DEFAULT_MAX_VALUE;
    private float mShowLeftFrom = DEFAULT_SHOW_LEFT_FROM;
    private float mPercent = DEFAULT_PERCENT;
    private int mStrokeWidth;
    private int mBorderWidth;

    private float mStartAngle;
    private int[] mFgColors = new int[]{0xffffe400, 0xffd8a043, 0xffff4800};
    private int mOuterColorStart = 0xff051d54;
    private int mOuterColorEnd = 0xff072065;
    private int mInnerColorStart = 0xff4aafd2;
    private int mInnerColorEnd = 0xff1c4ba8;
    private int mCircleIndicatorRadius;

    private Shader mShader;
    private LinearGradient mOuterShader;
    private LinearGradient mInnerShader;
    private RectF mOval = new RectF();
    private RectF mOuterBorder = new RectF();
    private RectF mInnerBorder = new RectF();
    private Paint mProgressPaint;
    private Paint mFillPaint;
    private Paint mBgPaint;
    private Paint mOuterBorderPaint;
    private Paint mInnerBorderPaint;
    private Paint mRoundCapPaint;
    private Paint mCircleIndicatorPaint;
    private Paint mTextPaint;
    private Paint.FontMetrics mTextFontMetrics;

    private int mSize;

    private ObjectAnimator animator;

    public ProgressView(Context context) {
        this(context, null);
    }

    public ProgressView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        int bgColor = 0xffe1e1e1;
        int fillColor = 0xffffe400;
        int circleIndicatorColor = Color.RED;
        int textColor = Color.WHITE;
        mStrokeWidth = dp2px(DEFAULT_STROKE_WIDTH);
        mBorderWidth = dp2px(DEFAULT_BORDER_WIDTH);
        mCircleIndicatorRadius = mStrokeWidth + mBorderWidth * 2;

        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs,
                    R.styleable.ProgressView,
                    0, 0);
            try {
                bgColor = a.getColor(R.styleable.ProgressView_bgColor, bgColor);
                fillColor = a.getColor(R.styleable.ProgressView_fillColor, fillColor);
                circleIndicatorColor = a.getColor(R.styleable.ProgressView_circleIndicatorColor, circleIndicatorColor);
                textColor = a.getColor(R.styleable.ProgressView_textColor, textColor);
                mFgColors[0] = a.getColor(R.styleable.ProgressView_fgColorStart, mFgColors[0]);
                mFgColors[1] = a.getColor(R.styleable.ProgressView_fgColorMiddle, mFgColors[1]);
                mFgColors[2] = a.getColor(R.styleable.ProgressView_fgColorEnd, mFgColors[2]);
                mOuterColorStart = a.getColor(R.styleable.ProgressView_outerBorderColorStart, mOuterColorStart);
                mOuterColorEnd = a.getColor(R.styleable.ProgressView_outerBorderColorEnd, mOuterColorEnd);
                mInnerColorStart = a.getColor(R.styleable.ProgressView_innerBorderColorStart, mInnerColorStart);
                mInnerColorEnd = a.getColor(R.styleable.ProgressView_innerBorderColorEnd, mInnerColorEnd);

                mMax = Math.max(a.getInteger(R.styleable.ProgressView_max, mMax), 1);
                if (a.hasValue(R.styleable.ProgressView_progress)) {
                    int progress = Math.max(0, Math.min(mMax, a.getInteger(R.styleable.ProgressView_progress, mMax / 2)));
                    mPercent = 100f * progress / mMax;
                } else {
                    mPercent = Math.min(100, a.getFloat(R.styleable.ProgressView_percent, mPercent));
                }

                mShowLeftFrom = Math.min(Math.max(a.getFloat(R.styleable.ProgressView_showLeftFrom, mShowLeftFrom), 0), mMax);
                mStartAngle = Math.max(0, Math.min(360, a.getFloat(R.styleable.ProgressView_startAngle, mStartAngle)));
                mStrokeWidth = a.getDimensionPixelSize(R.styleable.ProgressView_strokeWidth, mStrokeWidth);
                mBorderWidth = a.getDimensionPixelSize(R.styleable.ProgressView_borderWidth, mBorderWidth);
                mCircleIndicatorRadius = a.getDimensionPixelSize(R.styleable.ProgressView_circleIndicatorRadius, mCircleIndicatorRadius);
            } finally {
                a.recycle();
            }
        }

        mFillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setColor(fillColor);

        mBgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBgPaint.setStyle(Paint.Style.FILL);
        mBgPaint.setColor(bgColor);

        mCircleIndicatorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCircleIndicatorPaint.setColor(circleIndicatorColor);
        mCircleIndicatorPaint.setStyle(Paint.Style.FILL);

        mTextPaint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(textColor);
        mTextPaint.setTextSize(mCircleIndicatorRadius / 1.1f);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextFontMetrics = mTextPaint.getFontMetrics();

        mProgressPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mProgressPaint.setStyle(Paint.Style.STROKE);
        mProgressPaint.setStrokeWidth(mStrokeWidth);
        mProgressPaint.setStrokeCap(Paint.Cap.ROUND);

        mOuterBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mOuterBorderPaint.setStyle(Paint.Style.STROKE);
        mOuterBorderPaint.setStrokeWidth(mBorderWidth);

        mInnerBorderPaint = new Paint(mOuterBorderPaint);

        mRoundCapPaint = new Paint(mProgressPaint);
        mRoundCapPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    private int dp2px(float dp) {
        return (int) (getContext().getResources().getDisplayMetrics().density * dp + 0.5f);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float percent = mPercent;

        canvas.drawArc(mOuterBorder, 0, 360, false, mBgPaint);
        canvas.drawArc(mInnerBorder, 0, 360, false, mFillPaint);

        canvas.drawArc(mOuterBorder, 0, 360, false, mOuterBorderPaint);
        canvas.drawArc(mInnerBorder, 0, 360, false, mInnerBorderPaint);

        // draw progress bar
        canvas.save();
        canvas.rotate(-90 + mStartAngle, mOval.centerX(), mOval.centerY());
        canvas.drawArc(mOval, 0, percent * 3.6f, false, mProgressPaint);
        mRoundCapPaint.setColor(mFgColors[0]);
        canvas.drawArc(mOval, 0, 0.1f, false, mRoundCapPaint);
        canvas.restore();

        // draw circle indicator
        int progress = getProgress();
        if (progress >= mMax - mShowLeftFrom) {
            canvas.save();
            canvas.rotate(mStartAngle + percent * 3.6f, mOval.centerX(), mOval.centerY());
            canvas.rotate(-mStartAngle - percent * 3.6f, mOval.centerX(), mOval.top);
            canvas.drawCircle(mOval.centerX(), mOval.top, mCircleIndicatorRadius, mCircleIndicatorPaint);
            canvas.drawText(Integer.toString(mMax - progress), mOval.centerX(), mOval.top + mTextFontMetrics.bottom, mTextPaint);
            canvas.restore();
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        updateShape();
    }

    private void updateShape() {
        // Fixes possible errors when some public overridden methods are called before init (in API 16 where setPadding is called)
        if (!isInited()) return;

        mSize = Math.min(getWidth() - getPaddingLeft() - getPaddingRight(), getHeight() - getPaddingTop() - getPaddingBottom());
        float halfSize = mSize / 2;
        float xCenter = getPaddingLeft() + (getWidth() - getPaddingLeft() - getPaddingRight()) / 2;
        float yCenter = getPaddingTop() + (getHeight() - getPaddingTop() - getPaddingBottom()) / 2;
        float left = Math.max(xCenter - halfSize, 0);
        float top = Math.max(yCenter - halfSize, 0);
        float right = Math.max(xCenter + halfSize, left);
        float bottom = Math.max(yCenter + halfSize, top);
        float halfBW = mBorderWidth / 2;
        float halfSWShift = mStrokeWidth / 2 + halfBW;
        mOuterBorder.set(left + halfBW, top + halfBW, right - halfBW, bottom - halfBW);
        mOval.set(mOuterBorder.left + halfSWShift, mOuterBorder.top + halfSWShift,
                mOuterBorder.right - halfSWShift, mOuterBorder.bottom - halfSWShift);
        mInnerBorder.set(mOval.left + halfSWShift, mOval.top + halfSWShift,
                mOval.right - halfSWShift, mOval.bottom - halfSWShift);

        // update shaders
        mShader = new SweepGradient(mOval.centerX(), mOval.centerY(), mFgColors, SWEEP_POSITIONS);
        mProgressPaint.setShader(mShader);
        mOuterShader = prepareShader(mOuterBorder, mOuterColorStart, mOuterColorEnd);
        mOuterBorderPaint.setShader(mOuterShader);
        mInnerShader = prepareShader(mInnerBorder, mInnerColorStart, mInnerColorEnd);
        mInnerBorderPaint.setShader(mInnerShader);
    }

    private LinearGradient prepareShader(RectF rect, int colorStart, int colorEnd) {
        return new LinearGradient(rect.left, rect.top, rect.left, rect.bottom, colorStart, colorEnd, Shader.TileMode.CLAMP);
    }

    @Override
    public void setPadding(int left, int top, int right, int bottom) {
        super.setPadding(left, top, right, bottom);
        if (isInited()) {
            updateShape();
            refreshTheLayout();
        }
    }

    @Override
    public void setPaddingRelative(int start, int top, int end, int bottom) {
        super.setPaddingRelative(start, top, end, bottom);
        if (isInited()) {
            updateShape();
            refreshTheLayout();
        }
    }

    public float getPercent() {
        return mPercent;
    }

    public void setPercent(float mPercent) {
        if (this.mPercent != mPercent) {
            this.mPercent = mPercent;
            refreshTheLayout();
        }
    }

    public int getStrokeWidth() {
        return mStrokeWidth;
    }

    public void setStrokeWidth(int mStrokeWidth) {
        this.mStrokeWidth = mStrokeWidth;
        mProgressPaint.setStrokeWidth(mStrokeWidth);
        updateShape();
        refreshTheLayout();
    }

    public void setStrokeWidthDp(float dp) {
        setStrokeWidth(dp2px(dp));
    }

    public void refreshTheLayout() {
        invalidate();
        requestLayout();
    }

    public void setFgColor(int mFgColorStart, int mFgColorMiddle, int mFgColorEnd) {
        boolean changed = false;
        if (mFgColors[0] != mFgColorStart) {
            mFgColors[0] = mFgColorStart;
            changed = true;
        }
        if (mFgColors[1] != mFgColorMiddle) {
            mFgColors[1] = mFgColorMiddle;
            changed = true;
        }
        if (mFgColors[2] != mFgColorEnd) {
            mFgColors[2] = mFgColorEnd;
            changed = true;
        }
        if (changed) {
            mShader = new SweepGradient(mOval.centerX(), mOval.centerY(), mFgColors, SWEEP_POSITIONS);
            mProgressPaint.setShader(mShader);
            refreshTheLayout();
        }
    }

    public void setFgColor(int[] colors) {
        if (colors.length != 3) {
            throw new IllegalArgumentException(getContext().getString(R.string.color_length_error, colors.length));
        }
        setFgColor(colors[0], colors[1], colors[2]);
    }

    public int getFgColorStart() {
        return mFgColors[0];
    }

    public void setFgColorStart(int mFgColorStart) {
        setFgColor(mFgColorStart, mFgColors[1], mFgColors[2]);
    }

    public int getFgColorMiddle() {
        return mFgColors[1];
    }

    public void setFgColorMiddle(int mFgColorMiddle) {
        setFgColor(mFgColors[0], mFgColorMiddle, mFgColors[2]);
    }

    public int getFgColorEnd() {
        return mFgColors[2];
    }

    public void setFgColorEnd(int mFgColorEnd) {
        setFgColor(mFgColors[0], mFgColors[1], mFgColorEnd);
    }


    public float getStartAngle() {
        return mStartAngle;
    }

    public void setStartAngle(float startAngle) {
        this.mStartAngle = startAngle;
        refreshTheLayout();
    }

    public void animateIndeterminate() {
        animateIndeterminate(800, new AccelerateDecelerateInterpolator());
    }

    public void animateIndeterminate(int durationOneCircle,
                                     TimeInterpolator interpolator) {
        animator = ObjectAnimator.ofFloat(this, "startAngle", getStartAngle(), getStartAngle() + 360);
        if (interpolator != null) animator.setInterpolator(interpolator);
        animator.setDuration(durationOneCircle);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setRepeatMode(ValueAnimator.RESTART);
        animator.start();
    }

    public void stopAnimateIndeterminate() {
        if (animator != null) {
            animator.cancel();
            animator = null;
        }
    }

    public void setProgress(int progress) {
        if (progress < 0 || progress > mMax) {
            throw new InvalidParameterException(getContext().getString(R.string.progress_not_in_range));
        }
        setPercent(100f * progress / mMax);
    }

    public int getProgress() {
        return Math.round(mMax * mPercent / 100 + 0.5f) - 1;
    }

    public int getSize() {
        return mSize;
    }

    public int getMax() {
        return mMax;
    }

    public void setMax(int max) {
        mMax = max;
        refreshTheLayout();
    }

    public float getShowLeftFrom() {
        return mShowLeftFrom;
    }

    public void setShowLeftFrom(float showLeftFrom) {
        mShowLeftFrom = showLeftFrom;
        refreshTheLayout();
    }

    public int getBorderWidth() {
        return mBorderWidth;
    }

    public void setBorderWidth(int borderWidth) {
        mBorderWidth = borderWidth;
        updateShape();
        refreshTheLayout();
    }

    public int getBgColor() {
        return mBgPaint.getColor();
    }

    public void setBgColor(int bgColor) {
        mBgPaint.setColor(bgColor);
        refreshTheLayout();
    }

    public int getFillColor() {
        return mFillPaint.getColor();
    }

    public void setFillColor(int fillColor) {
        mFillPaint.setColor(fillColor);
        refreshTheLayout();
    }

    public int getOuterColorStart() {
        return mOuterColorStart;
    }

    public void setOuterColorStart(int outerColorStart) {
        mOuterColorStart = outerColorStart;
        refreshTheLayout();
    }

    public int getOuterColorEnd() {
        return mOuterColorEnd;
    }

    public void setOuterColorEnd(int outerColorEnd) {
        mOuterColorEnd = outerColorEnd;
        refreshTheLayout();
    }

    public int getInnerColorStart() {
        return mInnerColorStart;
    }

    public void setInnerColorStart(int innerColorStart) {
        mInnerColorStart = innerColorStart;
        refreshTheLayout();
    }

    public int getInnerColorEnd() {
        return mInnerColorEnd;
    }

    public void setInnerColorEnd(int innerColorEnd) {
        mInnerColorEnd = innerColorEnd;
        refreshTheLayout();
    }

    public int getCircleIndicatorColor() {
        return mCircleIndicatorPaint.getColor();
    }

    public void setCircleIndicatorColor(int circleIndicatorColor) {
        mCircleIndicatorPaint.setColor(circleIndicatorColor);
        refreshTheLayout();
    }

    public int getTextColor() {
        return mTextPaint.getColor();
    }

    public void setTextColor(int textColor) {
        mTextPaint.setColor(textColor);
        refreshTheLayout();
    }

    public int getCircleIndicatorRadius() {
        return mCircleIndicatorRadius;
    }

    public void setCircleIndicatorRadius(int circleIndicatorRadius) {
        mCircleIndicatorRadius = circleIndicatorRadius;
        refreshTheLayout();
    }

    private boolean isInited() {
        return mProgressPaint != null;
    }
}
