package com.qvolta.security

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import com.j256.twofactorauth.TimeBasedOneTimePasswordUtil
import com.qvolta.security.ui.ScanQRFragment
import com.qvolta.security.ui.TOTPFragment
import io.fabric.sdk.android.Fabric


class MainActivity : AppCompatActivity(), ITOTPListener {

    private val pattern = "^(?:[A-Z2-7]{8})*(?:[A-Z2-7]{2}={6}|[A-Z2-7]{4}={4}|[A-Z2-7]{5}={3}|[A-Z2-7]{7}=)?\$"
    private val regex by lazy { Regex(pattern) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_main)

        val seed = load()

        val fragment = if (seed.isNotEmpty()) TOTPFragment() else ScanQRFragment()

        if (savedInstanceState == null)
            supportFragmentManager.beginTransaction()
                    .replace(R.id.content, fragment, "front")
                    .commitAllowingStateLoss()

    }

    override fun save(seed: String) {
        getSharedPreferences("qvolta", Context.MODE_PRIVATE)
                .edit()
                .putString("seed", seed)
                .apply()
    }

    override fun generate() = TimeBasedOneTimePasswordUtil.generateNumberString(load(), System.currentTimeMillis(), 60)

    override fun load() = getSharedPreferences("qvolta", Context.MODE_PRIVATE).getString("seed", "")

    override fun valid(seed: String) = regex.containsMatchIn(seed)

}

interface ITOTPListener {
    fun generate(): String
    fun save(seed: String)
    fun load(): String
    fun valid(seed: String): Boolean
}
