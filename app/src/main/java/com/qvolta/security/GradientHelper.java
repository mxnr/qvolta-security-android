package com.qvolta.security;

import android.graphics.Color;

/**
 * Created by Serhii Kamenkovych on 10.02.2018.
 */

public class GradientHelper {

    private final int max;
    private int[] colors;
    private float[] positions;
    private static final float QUARTER = 25;
    private static final float HALF = 50;
    private static final float FULL = 100;

    public GradientHelper(int max, int firstQuarterColor, int halfColor, int endColor) {
        this.max = max;
        colors = new int[]{firstQuarterColor, halfColor, endColor};
        positions = new float[]{0, HALF, FULL};
    }

    public int[] getColors(float position) {
        float v = position * FULL / max;
        return new int[]{getStartColorProgress(v), getMiddleColorProgress(v), getEndColorProgress(v)};
    }

    public int getStartColor(float position) {
        float v = position * FULL / max;
        return getStartColorProgress(v);
    }

    private int getStartColorProgress(float v) {
        if (v < HALF) {
            return colors[0];
        }
        return getColorFromGradient(colors, positions, (v - HALF) * 1.5f);
    }

    public int getMiddleColor(float position) {
        float v = position * FULL / max;
        return getMiddleColorProgress(v);
    }

    private int getMiddleColorProgress(float v) {
        if (v < QUARTER) {
            return colors[0];
        }
        if (v < HALF) return getColorFromGradient(colors, positions, (v - QUARTER) * 2);
        return getColorFromGradient(colors, positions, (v - HALF) / 2 + HALF);
    }

    public int getEndColor(float position) {
        return getEndColorProgress(FULL);
    }

    private int getEndColorProgress(float v) {
        return colors[2];
    }

    public static int getColorFromGradient(int[] colors, float[] positions, float v) {

        if (colors.length == 0 || colors.length != positions.length) {
            throw new IllegalArgumentException();
        }

        if (colors.length == 1) {
            return colors[0];
        }

        if (v <= positions[0]) {
            return colors[0];
        }

        if (v >= positions[positions.length - 1]) {
            return colors[positions.length - 1];
        }

        for (int i = 1; i < positions.length; ++i) {
            if (v <= positions[i]) {
                float t = (v - positions[i - 1]) / (positions[i] - positions[i - 1]);
                return lerpColor(colors[i - 1], colors[i], t);
            }
        }

        //should never make it here
        throw new RuntimeException();
    }

    public static int lerpColor(int colorA, int colorB, float t) {
        int alpha = (int) Math.floor(Color.alpha(colorA) * (1 - t) + Color.alpha(colorB) * t);
        int red = (int) Math.floor(Color.red(colorA) * (1 - t) + Color.red(colorB) * t);
        int green = (int) Math.floor(Color.green(colorA) * (1 - t) + Color.green(colorB) * t);
        int blue = (int) Math.floor(Color.blue(colorA) * (1 - t) + Color.blue(colorB) * t);

        return Color.argb(alpha, red, green, blue);
    }

}
