package com.qvolta.security.ui

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.widget.Button
import android.widget.Toast
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.qvolta.security.ITOTPListener
import com.qvolta.security.R

/**
 * Created by egorsarnavsky on 30.01.2018.
 */


class QRReaderFragment : Fragment() {

    lateinit var seedProtocol: ITOTPListener

    lateinit var cameraView: SurfaceView
    lateinit var detector: BarcodeDetector
    lateinit var cameraSource: CameraSource

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cameraView = view.findViewById(R.id.surfaceView)

        detector = BarcodeDetector.Builder(context)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build()


        cameraSource = CameraSource.Builder(context, detector)
                .setRequestedPreviewSize(1600, 1024)
                .setAutoFocusEnabled(true)
                .build()

        cameraView.holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)

        cameraView.holder.addCallback(object : SurfaceHolder.Callback {

            @SuppressLint("MissingPermission")
            override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {
                cameraSource.start(cameraView.holder)

            }

            override fun surfaceDestroyed(p0: SurfaceHolder?) {
                cameraSource.stop()
            }

            override fun surfaceCreated(p0: SurfaceHolder?) {
            }
        })

        detector.setProcessor(object : Detector.Processor<Barcode> {

            override fun release() {
            }

            override fun receiveDetections(detection: Detector.Detections<Barcode>) {

                if (detection.detectedItems.size() > 0)
                    if (seedProtocol.valid(detection.detectedItems.valueAt(0).displayValue)) {
                        synchronized(this) {
                            detector.release()
                            activity.runOnUiThread {
                                val v = detection.detectedItems.valueAt(0).displayValue
                                seedProtocol.save(v)

                                repeat(fragmentManager.backStackEntryCount) {
                                    fragmentManager.popBackStack()
                                }
                                fragmentManager.beginTransaction()
                                        .replace(R.id.content, TOTPFragment(), "front")
                                        .commitAllowingStateLoss()
                            }
                        }
                    } else {
                        Toast.makeText(activity, getString(R.string.valid_code_is_not_found), Toast.LENGTH_LONG).show()
                    }
            }

        })

        view.findViewById<Button>(R.id.cancel).setOnClickListener { activity?.onBackPressed() }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.seedProtocol = context as ITOTPListener
    }

    override fun onDestroy() {
        super.onDestroy()
        detector.release()
        cameraSource.release()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.camera_layout, container, false)
}