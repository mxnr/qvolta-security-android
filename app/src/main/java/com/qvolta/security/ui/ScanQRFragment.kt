package com.qvolta.security.ui

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.qvolta.security.ITOTPListener
import com.qvolta.security.R

/**
 * Created by egorsarnavsky on 30.01.2018.
 */


class ScanQRFragment : Fragment() {

    lateinit var seedProtocol: ITOTPListener
    lateinit var back: ImageView
    lateinit var scan: Button
    lateinit var restore: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back = view.findViewById(R.id.back)
        back.setOnClickListener { activity?.onBackPressed() }

        if (seedProtocol.load().isEmpty()) back.visibility = View.INVISIBLE

        scan = view.findViewById(R.id.scan)

        val service = GoogleApiAvailability.getInstance()

        scan.setOnClickListener {
            if (service.isGooglePlayServicesAvailable(context) == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
                if (service.isUserResolvableError(service.isGooglePlayServicesAvailable(context)))
                    service.getErrorDialog(activity, service.isGooglePlayServicesAvailable(context), 2404).show()
            } else {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), 101)
                } else {
                    qrReader()
                }
            }
        }

        restore = view.findViewById(R.id.restore)
        restore.setOnClickListener {
            fragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out,
                            android.R.animator.fade_in, android.R.animator.fade_out)
                    .replace(R.id.content, RestoreFragment(), "restoreFragment")
                    .addToBackStack("")
                    .commitAllowingStateLoss()
        }
    }


    fun qrReader() {
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out,
                        android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(R.id.content, QRReaderFragment())
                .addToBackStack("")
                .commitAllowingStateLoss()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.seedProtocol = context as ITOTPListener
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101 && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            qrReader()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.scan_qr_layout, container, false)
}