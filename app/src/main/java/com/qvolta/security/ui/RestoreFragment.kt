package com.qvolta.security.ui

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import com.qvolta.security.ITOTPListener
import com.qvolta.security.R

/**
 * Created by egorsarnavsky on 30.01.2018.
 */


class RestoreFragment : Fragment() {

    private lateinit var seedProtocol: ITOTPListener

    private val VIEW_LVL_NO_ERROR = 0
    private val VIEW_LVL_ERROR = 1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val back = view.findViewById<ImageView>(R.id.back)
        back.setOnClickListener { activity?.onBackPressed() }

        val error: LinearLayout = view.findViewById(R.id.error)
        error show false
        error.setOnClickListener { it show false }

        val code = view.findViewById<EditText>(R.id.code)

        val clear = view.findViewById<ImageView>(R.id.clear)
        clear show false
        clear.setOnClickListener { code.text.clear() }

        val request = view.findViewById<Button>(R.id.request)
        request.isEnabled = false
        request.setOnClickListener {
            val seed = code.text.toString()
            if (seedProtocol.valid(seed)) {
                seedProtocol.save(seed.trim())

                repeat(fragmentManager.backStackEntryCount) {
                    fragmentManager.popBackStack()
                }
                fragmentManager.beginTransaction()
                        .replace(R.id.content, TOTPFragment(), "front")
                        .commitAllowingStateLoss()
            } else {
                error show true
                code.background.level = VIEW_LVL_ERROR
            }
        }

        code.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                request.isEnabled = s.isNotEmpty()
                clear show s.isNotEmpty()
                error show false
                code.background.level = VIEW_LVL_NO_ERROR
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.seedProtocol = context as ITOTPListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.restore_layout, container, false)

}

infix fun View.show(show: Boolean) {
    this.visibility = if (show) View.VISIBLE else View.GONE
}