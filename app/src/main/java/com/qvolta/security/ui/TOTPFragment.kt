package com.qvolta.security.ui

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.qvolta.security.GradientHelper
import com.qvolta.security.ITOTPListener
import com.qvolta.security.ProgressView
import com.qvolta.security.R
import java.util.*


/**
 * Created by egorsarnavsky on 30.01.2018.
 */


class TOTPFragment : Fragment() {

    private lateinit var progressView: ProgressView
    private lateinit var code: TextView
    private lateinit var another: TextView
    private lateinit var gradientHelper: GradientHelper

    lateinit var seedProtocol: ITOTPListener

    private var timer: Timer? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progressView = view.findViewById(R.id.crpv)
        code = view.findViewById(R.id.codeLayout)
        another = view.findViewById(R.id.another)

        var i = 0
        view.findViewById<ImageView>(R.id.imageView)
                .setOnClickListener {
                    i++
                    if (i == 10) Toast.makeText(context, "eGorets was here. 30.01.2018", Toast.LENGTH_SHORT).show()
                }


        another.setOnClickListener {
            fragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out,
                            android.R.animator.fade_in, android.R.animator.fade_out)
                    .replace(R.id.content, ScanQRFragment())
                    .addToBackStack("")
                    .commitAllowingStateLoss()
        }

        val start = Color.parseColor("#10d944")
        val middle = Color.parseColor("#e19f26")
        val end = Color.parseColor("#e93f3f")

        gradientHelper = GradientHelper(60, start, middle, end)
    }

    override fun onStart() {
        super.onStart()
        setCode()
        startTimer()
    }

    private fun setCode() {
        code.text = seedProtocol.generate()
    }

    private fun startTimer() {
        timer?.cancel()
        timer = kotlin.concurrent.timer(
                startAt = Date(),
                period = 10,
                action = {
                    activity.runOnUiThread(updateProgress)
                })
    }

    val updateProgress = {
        val p = progressView.progress
        progressView.percent = System.currentTimeMillis() % 60000 / 600F
        val n = progressView.progress
        if (n == 0 && n != p) setCode()

        val seconds = System.currentTimeMillis() % 60000 / 1000F
        progressView.setFgColor(gradientHelper.getColors(seconds))
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.seedProtocol = context as ITOTPListener
    }

    override fun onStop() {
        timer?.cancel()
        super.onStop()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.totp_layout, container, false)

}